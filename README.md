# NovInit - First Init Script
![](https://img.shields.io/badge/version-0.0.2-blue)
![](https://img.shields.io/badge/license-AGPLv3-blue)
```
 _   _          _____       _ _   
| \ | |        |_   _|     (_) |  
|  \| | _____   _| |  _ __  _| |_ 
| . ` |/ _ \ \ / / | | '_ \| | __|
| |\  | (_) \ V /| |_| | | | | |_ 
|_| \_|\___/ \_/_____|_| |_|_|\__|
                                  
                                  
NovInit v0.1.2
Copyright (C) 2023 Noveria Network

This program may be freely redistributed under the terms of the GNU AGPLv3
```
A quick shell script, that sets up a server with the needed packages and users.

Currently optimized for Fedora Server with a BTRFS filesystem.
It's mandatory to execute this script as `root` (or with `sudo`). The default shell of each user should be `/bin/zsh`

## Usage
```
usage: novinit [operation] <SSH PUBLIC KEY>
operations:
        novinit {-h --help}
        novinit {-V --version}
        novinit {-v --verbose}
        novinit {-n --dry-run}
```

The public key is a required parameter.
It will add it to the `authorized_keys` of the `root` user, since SSH password auth will be deactivated automatically because of security reasons.