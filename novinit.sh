#!/bin/zsh

#
# CONSTANTS
#
PKGVER="0.1.2"
LICENSE="GNU AGPLv3"
HELPERFILESPATH="./helper"
DEPENDENCIES=(
    "podman"
    "cockpit-podman"
    "podman-docker"
    "podman-compose"
    "mariadb-server"
    "java-latest-openjdk-headless"
)
SUBVOLUMES=(
    "/var/lib/mysql:mysql"
    "/opt/podman"
)
SERVICES=(
    "podman"
    "mariadb"
    "sshd"
)
USERS=(
    "noveria:/bin/false"
)

#
# VARIABLES
#
dryrun=false

#
# description: Prints a quick help page, on how to use this script
# usage: novinit -h/--help
#
function novinit() {

    print -P "usage: $0 [operation] <SSH PUBLIC KEY>\noperations:\n\t$0 {-h --help}\n\t$0 {-V --version}\n\t$0 {-v --verbose}\n\t$0 {-n --dry-run}"

}

#
# description: Prints the current version and license of the script
# usage: novinit -V/--version
#
function version() {
    if ! command -v figlet &> /dev/null; then
        source "$HELPERFILESPATH"/distrohelper.sh
        installPkg "figlet" &> /dev/null
    fi

    figlet -f big NovInit
    print -P "NovInit v$PKGVER\nCopyright (C) 2023 Noveria Network\n\nThis program may be freely redistributed under the terms of the $LICENSE"
}

function cleanup() {
    checkAction $0
    [ "$?" -eq 1 ] && return 0

    log d "Uninstalling pre-installed software"
    "$dryrun" && log i "Would've uninstalled: $DEPENDENCIES" || uninstallPkg "$DEPENDENCIES"

    for volume in $SUBVOLUMES; do
        local parsed=$(echo $volume | awk -F\: '{print $1}')
        if [[ "$dryrun" == false ]]; then
            log d "Checking if $parsed exists!"
            if [[ -d "$parsed" ]]; then
                log i "Removing $(basename $parsed) in path $(dirname $parsed)"
                if [ "$(stat --format=%i $parsed)" -eq 256 ]; then
                    executeCommand "btrfs subvolume delete $parsed 2> /dev/null"
                else
                    executeCommand "rm -rvf $parsed 2> /dev/null"
                fi
            fi
        fi
    done
    [[ "$dryrun" == false ]] && log s "Sucessfully cleaned up system!" || return 0
    addAction $0
}

#
# description: installs the dependencies, required for this script with the appropriate package manager for the distribution.
# Will be evaluated in distrohelper.sh
# usage: installDeps [...]
#
function installDeps() {
    checkAction "$0"
    [ "$?" -eq 1 ] && return 0

    log d "Updating System"
    "$dryrun" && log i "Would've updated the system" || updateSystem

    log d "Installing $DEPENDENCIES"
    "$dryrun" && log i "Would've installed: $DEPENDENCIES" || installPkg "$DEPENDENCIES"

    addAction "$0"
}

#
# description: creates the btrfs-subvolumes, which will be used for snapshotting.
# If they exist, the scripts aborts, because they were probably installed with the package.
# The : splits the path and the user, which will own the file. If no user is specified, the user will be root.
# The primary group of the user, will be evaluated with `id -gn <user>`
# usage: createSubVolume
#
function createSubVolume() {
    checkAction "$0"
    [ "$?" -eq 1 ] && return 0

    local parsedVolumes=()

    for volume in $SUBVOLUMES; do
        log d "Parsing '$volume' with awk"
        local parsed=$(echo $volume | awk -F\: '{print $1}')
        log d "Checking existance of $parsed"
        if [[ "$dryrun" == false ]]; then
            if [[ -d "$parsed" ]]; then
                log e "Directory $parsed does exist already."
                log e "Please make sure, it doesn't exist! Backup data inside, before deleting $parsed!"
                log e "Aborting..."
                exit 1
            fi
        fi
        parsedVolumes+=("$parsed")
    done
    for volume in $parsedVolumes; do
        log d "Checking if parent of $volume is a btrfs filesystem"
        if [[ "$(stat -f -c %T $(dirname $volume))" != "btrfs" ]]; then
            if [[ "$dryrun" == false ]]; then
                log e "$(dirname $volume) is not a btrfs filesystem."
                log e "Detected filesystem: $(stat -f -c %T $(dirname $volume))"
                log e "Reformat with btrfs or skip $(dirname $volume)!"
                log e "Aborting..."
                exit 1
            else
                log w "$(dirname $volume) is not a btrfs filesystem."
                log w "Execution would've stopped here to fix this problem."
                log w "Proceeding process, because '-n' flag is set."
            fi
        fi
    done
    local createdVolumes=()
    for volume in $parsedVolumes; do
        log d "Creating $(basename $volume) in path $(dirname $volume)"
        if [[ "$dryrun" == true ]]; then
            log i "Would've created subvolume $volume"
        else
            executeCommand "btrfs subvolume create $volume"
            createdVolumes+=("$volume")
        fi
    done

    [[ "$dryrun" == false ]] && log s "Sucessfully created subvolumes $createdVolumes" || return 0
    addAction "$0"
}

#
# description: adds users, defined in $USERS with their login-shell
# The login-shell will always be 
#
function createUsers() {
    checkAction "$0"
    [ "$?" -eq 1 ] && return 0

    for user in $USERS; do
        log d "Parsing '$user' with awk"
        local parsedUser=$(echo $user | awk -F\: '{print $1}')
        local parsedShell=$(echo $user | awk -F\: '{print $2}')

        if [ -z "$parsedShell" ]; then
            log e "No login-shell defined. Please define a login, by adding ':shell' to the userlist above"
            log e "For example: '$parsedUser:/bin/false'"
            log e "Aborting..."
            exit 1
        fi

        if [ ! -z "$(cat /etc/passwd | grep $parsedUser)" ]; then
            if [[ "$dryrun" == false ]]; then
                log e "User $parsedUser does already exist!"
                log e "Aborting..."
                exit 1
            else
                log w "User $parsedUser does already exist!"
                log w "Execution would've stopped here to fix this problem."
                log w "Proceeding process, because '-n' flag is set."
            fi
        fi
        if [[ "$dryrun" == true ]]; then
            log i "Would've created user $parsedUser with $parsedShell as login-shell!"
        else
            executeCommand "useradd --btrfs-subvolume-home --create-home --user-group --shell $parsedShell $parsedUser 2> /dev/null"
        fi
    done
    [[ "$dryrun" == false ]] && addAction $0 || return 0
}

#
# description: settings filepermissions of subvolumes defined in $SUBVOLUMES
# owner of file is the defined user after :
# usage: setFilePermissions
#
function setFilePermissions() {
    checkAction "$0"
    [ "$?" -eq 1 ] && return 0

    for volume in $SUBVOLUMES; do
        log d "Parsing '$volume' with awk"
        local parsedVolume=$(echo $volume | awk -F\: '{print $1}')
        if [ -z "$(echo $volume | awk -F\: '{print $2}')" ]; then
            local parsedOwner="root"
        else
            local parsedOwner=$(echo $volume | awk -F\: '{print $2}')
        fi
        log d "Setting fileowner of $parsedVolume to $parsedOwner"
        local primaryGroup=$(id -gn $parsedOwner 2> /dev/null)

        if [[ "$dryrun" == true ]]; then
            log i "Would've set owner of $parsedVolume to $parsedOwner:$primaryGroup"
        else
            executeCommand "chown -R $parsedOwner:$primaryGroup $parsedVolume"
        fi
        
    done
    [[ "$dryrun" == false ]] && addAction "$0" || return 0
}

#
# description: restarts and enabled the listed services in $SERVICES
# usage manageServices
#
function manageServices() {
    checkAction "$0"
    [ "$?" -eq 1 ] && return 0

    for service in $SERVICES; do
        log d "Enabling service $service"
        "$dryrun" && log i "Would've enabled $service" || executeCommand "systemctl enable $service 2> /dev/null"

        log d "Starting service $service"
        "$dryrun" && log i "Would've started $service" || executeCommand "systemctl start $service 2> /dev/null"
    done
    [[ "$dryrun" == false ]] && addAction $0 || return 0
}

#
# description: matches the entered ssh-key with the regex.
# adds it to to root's authorized_keys database, if true.
# usage: setupSSH <SSH PUBLIC KEY>
#
function setupSSH() {
    checkAction "$0"
    [ "$?" -eq 1 ] && return 0

    if [[ ! -d /root/.ssh ]]; then
        log d "Creating directory /root/.ssh"
        "$dryrun" && log i "Would've created directory /root/.ssh" || mkdir -p /root/.ssh
    fi

    log d "Checking if entered SSH-Key matches regex"
    regex="^(ssh-dss AAAAB3NzaC1kc3|ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNT|sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb2|ssh-ed25519 AAAAC3NzaC1lZDI1NTE5|sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29t|ssh-rsa AAAAB3NzaC1yc2)[0-9A-Za-z+\/]+[=]{0,3}(\s.*)?$"
    if [[ "$@" =~ "$regex" ]]; then
        log i "SSH-KEY matches regex. Adding to authorized_keys of user root"
        if [[ -z "$(grep $@[2] /root/.ssh/authorized_keys 2> /dev/null)" ]]; then
            "$dryrun" && log i "Would've added SSH-Key to authorized_keys" || echo "$@" >> /root/.ssh/authorized_keys; executeCommand "chmod 600 /root/.ssh/authorized_keys"
        fi
    else
        log e "Your entered SSH-Key does not match the defined regex."
        log e "Please make sure, you've entered a public key, that's compatible with openssh."
        log e "Otherwise, create a keypar using 'ssh-keygen -t ed25519' and follow the instructions you see on screen."
        log e "Aborting..."
        exit 1
    fi
    [[ "$dryrun" == false ]] && log s "Successfully added public key to authorized_keys"

    log d "Checking if PermitRootLogin-value is commented out in /etc/ssh/sshd_config"
    if [[ ! -z "$(grep "^#PermitRootLogin" /etc/ssh/sshd_config | head -n 1)" ]]; then
        log i "PermitRootLogin is commented out, uncommenting"
        executeCommand "sed -i 's/#PermitRootLogin/PermitRootLogin/g' /etc/ssh/sshd_config"
    fi
    
    log d "Checking if PermitRootLogin-value is 'prohibit-password' in /etc/ssh/sshd_config"
    if [[ "$(grep "^PermitRootLogin" /etc/ssh/sshd_config | awk '{print $2}')" != "prohibit-password" ]]; then
        log d "Value is $(grep "^PermitRootLogin" /etc/ssh/sshd_config | awk '{print $2}')"
        "$dryrun" && log i "Would've set value to 'prohibit-password'" || log i "Setting value to 'prohibit-password'"
        [[ "$dryrun" == false ]] && awk '/^PermitRootLogin/ {
        found=1;
        sub(/^PermitRootLogin[[:blank:]]+.*$/, "PermitRootLogin prohibit-password")
        }
        1; 
        END {
        if (!found)
            print "PermitRootLogin prohibit-password"
        }' /etc/ssh/sshd_config >> /etc/ssh/sshd_config.new
        executeCommand "rm -f /etc/ssh/sshd_config; mv /etc/ssh/sshd_config.new /etc/ssh/sshd_config"
    fi

    log d "Checking if PubkeyAuthentication-value is commented out in /etc/ssh/sshd_config"
    if [[ ! -z "$(grep "^#PubkeyAuthentication" /etc/ssh/sshd_config | head -n 1)" ]]; then
        log i "PubkeyAuthentication is commented out, uncommenting"
        executeCommand "sed -i 's/#PubkeyAuthentication/PubkeyAuthentication/g' /etc/ssh/sshd_config"
    fi

    log d "Checking if PubkeyAuthentication-value is 'yes' in /etc/ssh/sshd_config"
    if [[ "$(grep "^PubkeyAuthentication" /etc/ssh/sshd_config | awk '{print $2}')" != "yes" ]]; then
        log d "Value is $(grep "^PubkeyAuthentication" /etc/ssh/sshd_config | awk '{print $2}')"
        "$dryrun" && log i "Would've set value to 'yes'" || log i "Setting value to 'yes'"
        [[ "$dryrun" == false ]] && awk '/^PubkeyAuthentication/ {
        found=1;
        sub(/^PubkeyAuthentication[[:blank:]]+.*$/, "PubkeyAuthentication yes")
        }
        1; 
        END {
        if (!found)
            print "PubkeyAuthentication yes"
        }' /etc/ssh/sshd_config >> /etc/ssh/sshd_config.new
        executeCommand "rm -f /etc/ssh/sshd_config; mv /etc/ssh/sshd_config.new /etc/ssh/sshd_config"
    fi

    [[ "$dryrun" == false ]] && log s "Sucessfully configured SSH" || return 0
    addAction "$0"
}

#
# description: This function locks every future execution of this script, since it's only supposed to run once.
# usage: lockFuture
#
function lockFuture() {
    mkdir -p /etc/novinit
    if [[ ! -f /etc/novinit/.lock ]]; then
        touch /etc/novinit/.lock
        chattr +i /etc/novinit/.lock
    fi
}

source $HELPERFILESPATH/scriptutil.sh

if [[ -f /etc/novinit/.lock ]]; then
    log e "The script has been locked for future executions, since it has been run successfully in the past"
    log e "To prevent any damage to the system, any executions will be blocked from now on!"
    log e "Aborting..."
    exit 1
fi

if [[ "$EUID" -ne 0 ]]; then
    log e "The script must be run as root!"
    log e "Aborting..."
    exit 1
fi

if [[ ! -z "$(ps auxwww | grep novinit | grep -v grep | grep -v $PWD | head -n2 | grep -v $(tty | sed 's/\/dev\///'))" ]]; then
    log e "There can't be multiple running instances of '$(echo $(basename $0) | awk -F"." '{print $1}')'!"
    log e "Aborting..."
    exit 1
fi

TEMP=$(getopt -o hVvn --long help,version,verbose,dry-run -n 'novinit' -- "$@")

if [ "$?" != 0 ] ; then log e "Aborting..." >&2 ; exit 1 ; fi

eval set -- "$TEMP"

while true; do
    case "$1" in
        -h | --help) novinit; exit 0 ;;
        -V | --version) version; exit 0 ;;
        -v | --verbose) debuglevel=4 ;;
        -n | --dry-run) dryrun=true;;
        -- ) shift; break;;
        * ) break ;;
    esac
    shift
done

if [[ -z "$@" ]]; then
    log e "You can't execute this script, without a public key. (Required for SSH root Login)"
    log e "Aborting..."
    novinit
    exit 1
fi

#
# IMPORTS
#
for file in $HELPERFILESPATH/*; do
    if [[ "$(basename $file)" != "scriptutil.sh" ]]; then
        log i "Importing $(basename $file)"
        source "$file"
    fi
done

#
# EXECUTING IN ORDER
#
cleanup
createSubVolume
installDeps
createUsers
setFilePermissions
setupSSH "$@"
manageServices
executeCommand "rm -f $ACTIONLISTFILE"
lockFuture