source /etc/os-release

function getDistro() {
    if [ -v ID_LIKE ]; then
        echo "$ID_LIKE"
        return 0
    fi
    echo "$ID"
}

function getPackageManager() {
    local pm
    case $1 in
        "arch")
            pm="pacman"
            ;;
        "fedora")
            pm="dnf"
            ;;
        "debian")
            pm="apt"
            ;;
    esac
    echo $pm
}

function installPkg() {
    local command
    case $pm in
        "pacman")
            command="pacman -S --noconfirm --needed"
            ;;
        "dnf")
            command="dnf install -y"
            ;;
        "apt")
            command="apt install -y"
            ;;
    esac
    for pkg in $@; do
        eval $command $pkg
    done
}

function uninstallPkg() {
    local command
    case $pm in
        "pacman")
            command="pacman -Rs --noconfirm"
            ;;
        "dnf")
            command="dnf remove -y"
            ;;
        "apt")
            command="apt purge -y"
            ;;
    esac
    for pkg in $@; do
        eval $command $pkg
    done
}

function updateSystem() {
    local command
    case $pm in
        "pacman")
            command="pacman -Syyu --noconfirm"
            ;;
        "dnf")
            command="dnf update -y"
            ;;
        "apt")
            command="apt purge -y"
            ;;
    esac
    eval $command
}
pm=$(getPackageManager $(getDistro))